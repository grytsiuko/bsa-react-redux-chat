import React from 'react';

import './index.css';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import MessageList from '../../components/MessageList';
import { IMessage, IMessageArray, isCurrentUserMessage } from '../../service/messageService';
import {
  getMessages,
  addMessage,
  updateMessage,
  deleteMessage,
  toggleLikeMessage,
  showEditModal,
  hideEditModal
} from './actions';
import { IRootState } from '../../store';
import Header from '../../components/Header';
import Spinner from '../../components/Spinner';
import MessageInput from '../../components/MessageInput';
import EditModal from '../../components/EditModal';

interface ChatStateProps {
  messages: IMessageArray | undefined,
  scrollOnRender: boolean | undefined,
  editingMessage: IMessage | undefined
}

interface ChatProps extends ChatStateProps {
  getMessages(): void,
  addMessage(text: string): void,
  updateMessage(message: IMessage): void
  toggleLikeMessage(message: IMessage): void
  deleteMessage(id: string): void,
  showEditModal(message: IMessage): void,
  hideEditModal(): void
}

const Chat: React.FunctionComponent<ChatProps> = (
  {
    messages,
    getMessages,
    addMessage,
    updateMessage,
    toggleLikeMessage,
    deleteMessage,
    showEditModal,
    hideEditModal,
    scrollOnRender,
    editingMessage
  }
) => {
  if (!messages) {
    getMessages();
  }

  const editLastMessage = (): void => {
    const currentUserMessages: IMessageArray = (messages || []).filter(m => isCurrentUserMessage(m));
    const { length } = currentUserMessages;
    if (length > 0) {
      showEditModal(currentUserMessages[length - 1]);
    }
  };

  return (
    <div className="chat-wrapper">
      <div className="chat">
        <Header messages={messages} />
        <MessageList
          messages={messages}
          deleteMessage={deleteMessage}
          toggleLikeMessage={toggleLikeMessage}
          showEditModal={showEditModal}
          scrollOnRender={scrollOnRender}
        />
        <MessageInput addMessage={addMessage} editLast={editLastMessage} />
        {editingMessage
        && <EditModal closeModal={hideEditModal} message={editingMessage} updateMessage={updateMessage} />}
        {!messages && <Spinner />}
      </div>
    </div>
  );
};

const mapStateToProps = (rootState: IRootState): ChatStateProps => ({
  messages: rootState.chat?.messages,
  scrollOnRender: rootState.chat?.scrollOnRender,
  editingMessage: rootState.chat?.editingMessage
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  getMessages: () => getMessages()(dispatch),
  addMessage: (text: string) => addMessage(text)(dispatch),
  updateMessage: (message: IMessage) => updateMessage(message)(dispatch),
  toggleLikeMessage: (message: IMessage) => toggleLikeMessage(message)(dispatch),
  deleteMessage: (id: string) => deleteMessage(id)(dispatch),
  showEditModal: (message: IMessage) => showEditModal(message)(dispatch),
  hideEditModal: () => hideEditModal()(dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
