import { AnyAction, Dispatch } from 'redux';
import * as messageService from '../../service/messageService';
import { IMessage, IMessageArray } from '../../service/messageService';
import {
  ADD_MESSAGE,
  DELETE_MESSAGE,
  LOAD_MESSAGES,
  SET_EDITING_MESSAGE,
  UPDATE_MESSAGE
} from './actionTypes';

export const loadMessagesAction = (messages: IMessageArray): AnyAction => ({
  type: LOAD_MESSAGES,
  messages,
  scrollOnRender: true
});

export const addMessageAction = (message: IMessage): AnyAction => ({
  type: ADD_MESSAGE,
  message,
  scrollOnRender: true
});

export const updateMessageAction = (message: IMessage): AnyAction => ({
  type: UPDATE_MESSAGE,
  message,
  scrollOnRender: false
});

export const deleteMessageAction = (id: string): AnyAction => ({
  type: DELETE_MESSAGE,
  id,
  scrollOnRender: false
});

export const toggleEditModalAction = (message: IMessage | undefined): AnyAction => ({
  type: SET_EDITING_MESSAGE,
  message,
  scrollOnRender: false
});

export const getMessages = () => async (dispatch: Dispatch): Promise<void> => {
  const loadedMessages: IMessageArray = await messageService.getMessages();
  loadedMessages.sort((a, b) => (a.createdAt > b.createdAt ? 1 : 0));
  dispatch(loadMessagesAction(loadedMessages));
};

export const addMessage = (text: string) => (dispatch: Dispatch): void => {
  const addedMessage: IMessage = messageService.addMessage(text);
  dispatch(addMessageAction(addedMessage));
};

export const updateMessage = (message: IMessage) => (dispatch: Dispatch): void => {
  const updatedMessage: IMessage = messageService.updateMessage(message);
  dispatch(updateMessageAction(updatedMessage));
};

export const toggleLikeMessage = (message: IMessage) => (dispatch: Dispatch): void => {
  const updatedMessage: IMessage = messageService.toggleLikeMessage(message);
  dispatch(updateMessageAction(updatedMessage));
};

export const deleteMessage = (id: string) => (dispatch: Dispatch): void => {
  dispatch(deleteMessageAction(id));
};

export const showEditModal = (message: IMessage) => (dispatch: Dispatch): void => {
  dispatch(toggleEditModalAction(message));
};

export const hideEditModal = () => (dispatch: Dispatch): void => {
  dispatch(toggleEditModalAction(undefined));
};
