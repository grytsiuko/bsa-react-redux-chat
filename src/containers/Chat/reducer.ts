import { AnyAction } from 'redux';
import { IMessage, IMessageArray } from '../../service/messageService';
import {
  ADD_MESSAGE,
  DELETE_MESSAGE,
  LOAD_MESSAGES,
  SET_EDITING_MESSAGE,
  UPDATE_MESSAGE
} from './actionTypes';

export interface IChatState {
  messages?: IMessageArray,
  scrollOnRender?: boolean,
  editingMessage?: IMessage
}

const chatReducer = (state: IChatState = {}, action: AnyAction) => {
  switch (action.type) {
    case LOAD_MESSAGES:
      return {
        ...state,
        messages: action.messages,
        scrollOnRender: action.scrollOnRender
      };
    case ADD_MESSAGE:
      return {
        ...state,
        messages: [...(state.messages || []), action.message],
        scrollOnRender: action.scrollOnRender
      };
    case UPDATE_MESSAGE:
      return {
        ...state,
        messages: (state.messages || []).map(m => (m.id === action.message.id ? action.message : m)),
        scrollOnRender: action.scrollOnRender
      };
    case DELETE_MESSAGE:
      return {
        ...state,
        messages: (state.messages || []).filter(m => m.id !== action.id),
        scrollOnRender: action.scrollOnRender
      };
    case SET_EDITING_MESSAGE:
      return {
        ...state,
        editingMessage: action.message,
        scrollOnRender: action.scrollOnRender
      };
    default:
      return state;
  }
};

export default chatReducer;
