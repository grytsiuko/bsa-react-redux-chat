import React from 'react';

import './index.css';
import { parseDateTime } from '../../utils/datetime';
import { IMessageArray } from '../../service/messageService';
import HeaderShortLabel from './shortLabel';
import HeaderLongLabel from './longLabel';

type HeaderProps = {
  messages: IMessageArray | undefined
}

const Header: React.FunctionComponent<HeaderProps> = ({ messages }) => {
  const participantsAmount: string = messages ? new Set(messages.map(item => item.userId)).size.toString() : '-';
  const messagesAmount: string = messages ? messages.length.toString() : '-';
  const lastMessage: string = messages ? parseDateTime(messages[messages.length - 1].createdAt) : '-';

  return (
    <div className="header">
      <div className="header-side">
        <h2 className="header-block header-title">TeleChat</h2>
        <HeaderShortLabel label={participantsAmount} text="participants" />
        <HeaderShortLabel label={messagesAmount} text="messages" />
      </div>
      <div className="header-side">
        <HeaderLongLabel label={lastMessage} text="last message at" />
      </div>
    </div>
  );
};

export default Header;
