import React, { useRef, useState, useEffect } from 'react';

import './index.css';
import Icon from '../Icon';
import { IMessage } from '../../service/messageService';
import ConfirmButton from '../ConfirmButton';
import SimpleButton from '../SimpleButton';

type EditModalProps = {
  message: IMessage,
  updateMessage(message: IMessage): void,
  closeModal(): void
}

const EditModal: React.FunctionComponent<EditModalProps> = ({ closeModal, message, updateMessage }) => {
  const [text, setText] = useState<string>(message.text);
  const modalRef = useRef<HTMLDivElement>(null);
  const textareaRef = useRef<HTMLTextAreaElement>(null);

  const handleChange = (value: string): void => {
    setText(value);
  };

  const handleUpdate = (): void => {
    if (text.trim()) {
      updateMessage({ ...message, text });
      closeModal();
    }
  };

  const handleTextAreaKeyPress = (e: React.KeyboardEvent<HTMLTextAreaElement>): void => {
    if (e.key === 'Enter' && !e.shiftKey) {
      e.preventDefault();
      handleUpdate();
    }
  };

  const handleKeyboardClose = (e: KeyboardEvent): void => {
    if (e.key === 'Escape') {
      closeModal();
    }
  };

  const handleMouseClose = (event: any) => {
    if (modalRef.current && !modalRef.current.contains(event.target)) {
      closeModal();
    }
  };

  const focusTextarea = (): void => {
    if (textareaRef.current) {
      // to focus on textarea and put cursor to the end
      textareaRef.current.focus();
      const tmp: string = textareaRef.current.innerHTML;
      textareaRef.current.value = '';
      textareaRef.current.value = tmp;
    }
  };

  useEffect(() => {
    focusTextarea();
    document.addEventListener('mousedown', handleMouseClose);
    document.addEventListener('keydown', handleKeyboardClose);
    return () => {
      document.removeEventListener('mousedown', handleMouseClose);
      document.removeEventListener('keydown', handleKeyboardClose);
    };
  }, [handleKeyboardClose, handleMouseClose, modalRef]);

  return (
    <div className="modal-wrapper">
      <div className="modal" ref={modalRef}>
        <div className="modal-header">
          <div className="modal-title">Edit message</div>
          <Icon icon="fas fa-times" active={false} callback={closeModal} />
        </div>
        <div className="modal-body">
          <textarea
            className="modal-textarea"
            onChange={e => handleChange(e.target.value)}
            onKeyDown={e => handleTextAreaKeyPress(e)}
            value={text}
            ref={textareaRef}
          />
        </div>
        <div className="modal-footer">
          <SimpleButton callback={() => closeModal()} title="Cancel" />
          <ConfirmButton callback={() => handleUpdate()} isBlocked={text.trim() === ''} title="Update" />
        </div>
      </div>
    </div>
  );
};

export default EditModal;
