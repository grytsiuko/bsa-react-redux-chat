import React from 'react';

import './index.css';
import { IMessage } from '../../service/messageService';
import MessageAvatar from './avatar';
import MessageContent from './content';
import Icon from '../Icon';

type MessageProps = {
  message: IMessage
  currentUserMessage: boolean,
  toggleLikeMessage(message: IMessage): void,
  deleteMessage(id: string): void,
  showEditModal(message: IMessage): void
}

const Message: React.FunctionComponent<MessageProps> = (
  {
    message,
    currentUserMessage,
    toggleLikeMessage,
    deleteMessage,
    showEditModal
  }
) => {
  const handleDelete = (): void => deleteMessage(message.id);
  const handleEdit = (): void => showEditModal(message);
  const handleToggleLike = (): void => toggleLikeMessage(message);

  const isLiked: boolean = message.liked || false;

  return (
    <div>
      <div className={`message-wrapper${currentUserMessage ? ' message-wrapper-right' : ''}`}>
        {currentUserMessage && <Icon icon="fas fa-trash" active={false} callback={handleDelete} />}
        {currentUserMessage && <Icon icon="fas fa-cog" active={false} callback={handleEdit} />}
        <div className="message">
          {!currentUserMessage && <MessageAvatar message={message} />}
          <MessageContent message={message} currentUserMessage={currentUserMessage} />
        </div>
        {!currentUserMessage && <Icon icon="fas fa-thumbs-up" active={isLiked} callback={handleToggleLike} />}
      </div>
    </div>
  );
};

export default Message;
