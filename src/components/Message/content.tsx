import React from 'react';

import { IMessage } from '../../service/messageService';
import { parseTime } from '../../utils/datetime';

type MessageContentProps = {
  message: IMessage
  currentUserMessage: boolean
}

const MessageContent: React.FunctionComponent<MessageContentProps> = ({ message, currentUserMessage }) => {
  const time: string = (message.editedAt === '')
    ? parseTime(message.createdAt)
    : `Edited at ${parseTime(message.editedAt)}`;

  return (
    <div className="message-content">
      {!currentUserMessage && <div className="message-user">{message.user}</div>}
      <div className="message-body">
        <div className="message-text">{message.text}</div>
        <div className="message-time">{time}</div>
      </div>
    </div>
  );
};

export default MessageContent;
