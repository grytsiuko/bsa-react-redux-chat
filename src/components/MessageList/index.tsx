import React, { useRef, useEffect } from 'react';

import './index.css';
import { IMessage, IMessageArray, isCurrentUserMessage } from '../../service/messageService';
import { parseDate, sameDay } from '../../utils/datetime';
import ListSeparator from '../ListSeparator';
import Message from '../Message';

type MessageListProps = {
  messages: IMessageArray | undefined,
  scrollOnRender: boolean | undefined,
  toggleLikeMessage(message: IMessage): void,
  deleteMessage(id: string): void,
  showEditModal(message: IMessage): void
}

const MessageList: React.FunctionComponent<MessageListProps> = (
  {
    messages,
    toggleLikeMessage,
    deleteMessage,
    showEditModal,
    scrollOnRender
  }
) => {
  const messagesEndRef = useRef<HTMLDivElement>(null);

  const scrollToBottom = () => {
    if (scrollOnRender && messagesEndRef?.current) {
      messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  useEffect(scrollToBottom);

  const buildListSeparator = (message: IMessage): JSX.Element => (
    <ListSeparator
      key={message.createdAt}
      title={parseDate(message.createdAt)}
    />
  );

  const buildMessage = (message: IMessage): JSX.Element => (
    <Message
      key={message.id}
      message={message}
      currentUserMessage={isCurrentUserMessage(message)}
      toggleLikeMessage={toggleLikeMessage}
      deleteMessage={deleteMessage}
      showEditModal={showEditModal}
    />
  );

  const buildMessageList = (): Array<JSX.Element> => {
    const list: Array<JSX.Element> = [];
    if (messages) {
      for (let i: number = 0; i < messages.length; i++) {
        const currMessage: IMessage = messages[i];
        if (i === 0 || !sameDay(messages[i - 1], currMessage)) {
          list.push(buildListSeparator(currMessage));
        }
        list.push(buildMessage(currMessage));
      }
    }
    return list;
  };

  return (
    <div className="message-list-wrapper">
      <div className="message-list">
        {buildMessageList()}
        <div ref={messagesEndRef} />
      </div>
    </div>
  );
};

export default MessageList;
