import React from 'react';

import './index.css';

type ConfirmButtonProps = {
  title: string,
  callback(): void,
  isBlocked: boolean
}

const ConfirmButton: React.FunctionComponent<ConfirmButtonProps> = ({ title, callback, isBlocked}) => {
  const className: string = `confirm-button button${isBlocked ? ' button-blocked' : ''}`;

  return (
    <button type="button" className={className} onClick={callback}>
      {title}
    </button>
  );
};

export default ConfirmButton;
