import { IMessage } from '../service/messageService';

export const parseDateTime = (dateTimeString: string): string => {
  const dateTimeObj: Date = new Date(dateTimeString);
  const date: number = dateTimeObj.getDate();
  const month: string = dateTimeObj.toLocaleString('default', { month: 'long' });
  const year: number = dateTimeObj.getFullYear();
  const hour: string = String(dateTimeObj.getHours()).padStart(2, '0');
  const minute: string = String(dateTimeObj.getMinutes()).padStart(2, '0');
  return `${date} ${month.substring(0, 3)} ${year}, ${hour}:${minute}`;
};

export const parseTime = (dateTimeString: string): string => {
  const dateTimeObj: Date = new Date(dateTimeString);
  const hour: string = String(dateTimeObj.getHours()).padStart(2, '0');
  const minute: string = String(dateTimeObj.getMinutes()).padStart(2, '0');
  return `${hour}:${minute}`;
};

export const sameDay = (first: IMessage, second: IMessage): boolean => {
  const firstObj: Date = new Date(first.createdAt);
  const secondObj: Date = new Date(second.createdAt);
  const datesEqual: boolean = firstObj.getDate() === secondObj.getDate();
  const monthsEqual: boolean = firstObj.getMonth() === secondObj.getMonth();
  const yearsEqual: boolean = firstObj.getFullYear() === secondObj.getFullYear();
  return datesEqual && monthsEqual && yearsEqual;
};

const getCurrentDateObj = (): Date => {
  const currDateTimeObj: Date = new Date();
  const currDate: number = currDateTimeObj.getDate();
  const currMonth: number = currDateTimeObj.getMonth();
  const currYear: number = currDateTimeObj.getFullYear();

  return new Date(currYear, currMonth, currDate);
};

export const parseDate = (dateTimeString: string): string => {
  const dateTimeObj: Date = new Date(dateTimeString);
  const date: number = dateTimeObj.getDate();
  const month: number = dateTimeObj.getMonth();
  const year: number = dateTimeObj.getFullYear();

  const ONE_DAY = 1000 * 60 * 60 * 24;
  const dateObj: Date = new Date(year, month, date);
  const currDateObj: Date = getCurrentDateObj();
  const differenceMs: number = currDateObj.getTime() - dateObj.getTime();

  switch (differenceMs) {
    case 0:
      return 'Today';
    case ONE_DAY:
      return 'Yesterday';
    default:
      const monthStr: string = dateTimeObj.toLocaleString('default', { month: 'long' });
      return `${date} ${monthStr} ${year}`;
  }
};
