import {combineReducers, createStore, PreloadedState} from 'redux';

import chatReducer, {IChatState} from "./containers/Chat/reducer";

export interface IRootState {
  chat?: IChatState
}

const initialState: PreloadedState<IRootState> = {};

const rootReducer = combineReducers({
  chat: chatReducer
});

const store = createStore(rootReducer, initialState);

export default store;
