interface IApiArgs {
  endpoint: string,
  method: string
}

export default async function callApi(args: IApiArgs): Promise<Response> {
  return fetch(
    args.endpoint,
    {
      method: args.method
    }
  );
}
